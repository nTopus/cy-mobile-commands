context('Navigate with visitMobile', () => {

  beforeEach(() => {
    cy.viewport('samsung-s10')
  })

  it('using only a single url string param', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
      .then(win => expect(win.ontouchstart).to.be.null)
  })

  it('using only the url option in a single object param', () => {
    cy.visitMobile({url: '/cypress/fixtures/book.html'})
      .then(win => expect(win.ontouchstart).to.be.null)
  })

  it('using a url string param and a config option with onBeforeLoad', () => {
    cy.visitMobile('/cypress/fixtures/book.html', {
        onBeforeLoad: win => win.FOO = 'BAR'
      })
      .then(win => {
        expect(win.ontouchstart).to.be.null
        expect(win.FOO).to.be.eq('BAR')
      })
  })

})
