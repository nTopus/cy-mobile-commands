context('Swipe Actions', () => {

  beforeEach(() => {
    cy.viewport('samsung-s10')
  })

  it('simple swipe', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(0px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(344px/)
    cy.get('#book1 .page1').swipe('right', 'left')
    cy.wait(200)
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(-344px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(0px/)
  })

  it('three steps swipe', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(0px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(344px/)
    cy.get('#book1 .page1').swipe('right', 'center', 'left')
    cy.wait(200)
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(-344px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(0px/)
  })

  it('5 fingers swipe', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
    cy.get('#book1 .page1').swipe(
      [[310,300], [310,320], [310,340], [310,360], [310,380]],
      [[50, 300], [50, 320], [50, 340], [50, 360], [50, 380]]
    )
    cy.get('circle.start-0[cx="310"][cy="300"]')
    cy.get('circle.start-1[cx="310"][cy="320"]')
    cy.get('circle.start-2[cx="310"][cy="340"]')
    cy.get('circle.start-3[cx="310"][cy="360"]')
    cy.get('circle.start-4[cx="310"][cy="380"]')
    cy.get('circle.end-0[cx="50"][cy="300"]')
    cy.get('circle.end-1[cx="50"][cy="320"]')
    cy.get('circle.end-2[cx="50"][cy="340"]')
    cy.get('circle.end-3[cx="50"][cy="360"]')
    cy.get('circle.end-4[cx="50"][cy="380"]')
  })

  it('complex steps swipe', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
    cy.get('#book1 .page1').swipe({delay: 1500, steps: 20}, [340,300], [20,300], 'bottomRight', 'top', 'bottomLeft', [340,300])
    cy.get('circle.start-0[cx="340"][cy="300"]')
    cy.get('circle.checkpoint-0[cx="20"][cy="300"]')
    cy.get('circle.checkpoint-0[cx="317"][cy="542"]')
    cy.get('circle.checkpoint-0[cx="180"][cy="166"]')
    cy.get('circle.checkpoint-0[cx="43"][cy="542"]')
    cy.get('circle.end-0[cx="340"][cy="300"]')
  })

  it('slow swipe', () => {
    cy.visitMobile('/cypress/fixtures/book.html')
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(0px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(344px/)
    cy.get('#book1 .page1').swipe({delay: 2000}, 'right', 'left')
    cy.wait(200)
    cy.get('#book1 .page1').should('have.attr', 'style').and('match', /translateX\(-344px/)
    cy.get('#book1 .page2').should('have.attr', 'style').and('match', /translateX\(0px/)
  })

  it.skip('swipe Vuetify', () => {
    cy.visitMobile('https://vuetifyjs.com/components/carousels')
    cy.get('#usage--1').scrollIntoView()
    cy.get('#usage--1 .v-window-item:visible').contains('Slide 1')
    cy.get('#usage--1 .v-window-item:visible').notContains('Slide 2')
    cy.get('#usage--1 .v-window-item:visible').swipe('right', 'left')
    cy.wait(500)
    cy.get('#usage--1 .v-window-item:visible').notContains('Slide 1')
    cy.get('#usage--1 .v-window-item:visible').contains('Slide 2')
    cy.get('#usage--1 .v-window-item:visible').swipe('left', 'right')
    cy.wait(500)
    cy.get('#usage--1 .v-window-item:visible').contains('Slide 1')
    cy.get('#usage--1 .v-window-item:visible').notContains('Slide 2')
  })

  it('slide jQuery slider', () => {
    cy.visitMobile('https://demos.jquerymobile.com/1.4.5/slider/')
    cy.get('#Highlight').scrollIntoView()
    cy.get('#slider-2 ~ .ui-slider-track .ui-slider-handle')
      .swipe('center', [100, 50])
    cy.get('#slider-2').then(input => expect(input[0].value).be.eq('11'))
    cy.get('#slider-2 ~ .ui-slider-track .ui-slider-handle')
      .swipe('center', [300, 50])
    cy.get('#slider-2').then(input => expect(input[0].value).be.eq('88'))
  })

  it.skip('pan and zoom leafletjs map', () => {
    cy.visitMobile('https://leafletjs.com/examples/mobile/example.html')
    cy.wait(200)
    cy.get('.leaflet-control-zoom-in').click()
    cy.get('.leaflet-proxy.leaflet-zoom-animated').should('have.attr', 'style')
      .and('eq', 'transform: translate3d(256px, 256px, 0px) scale(1);')
    cy.get('#map')
      .swipe({delay: 500}, 'center', [170,330])
      .swipe({delay: 1000}, [[180,280],[180,320]], [[180,100],[180,500]])
    cy.wait(200)
    cy.get('.leaflet-proxy.leaflet-zoom-animated').should('have.attr', 'style')
      .and('eq', 'transform: translate3d(1088px, 789px, 0px) scale(4);')
  })

})
