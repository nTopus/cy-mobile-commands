Changelog
=========

[0.3.0] - 2021-10-31
--------------------

### Changed
- Test for newest cypress versions.

### Fixed
- `this` is undefined on "do swipe" log. #7

[0.2.0] - 2020-07-01
--------------------

### Changed
- TypeScript friendly.
